module.exports = {
  '$#*! My Dad Says': 'S01E18',
  '1600 Penn': 'S01E13',
  '8 Simple Rules': 'S03E24',
  '9 Av 10 Nordmenn': 'S03E13',
  'American Dad!': 'S09E20',
  'Arrested Development': 'S01E22',
  'Awkward': 'S02E12',
  'Betas': 'S01E11',
  'Better Off Ted': 'S02E13',
  'Billions': 'S01E12',
  'Black Mirror': 'S02E03',
  'Bob\'s Burgers': 'S05E21',
  'Bron/Broen': 'S03E10',
  'Chuck': 'S05E13',
  'Community': 'S03E22',
  'Continuum': 'S01E10',
  'Cougar Town': 'S04E15',
  'Dads (2013)': 'S01E19',
  'Dama til': 'S02E07',
  'Derren Brown Investigates': 'S01E03',
  'Derren Brown: The Events': 'S01E05',
  'Derren Brown: The Heist': 'S01E01',
  'Derren Brown: The System': 'S01E01',
  'Derren Brown: Trick of the Mind': 'S03E06',
  'Derren Brown: Trick Or Treat': 'S02E06',
  'Diktaturet': 'S01E08',
  'Elementary': 'S01E24',
  'Episodes': 'S04E09',
  'Family Guy': 'S14E20',
  'Friends': 'S10E18',
  'Futurama': 'S07E26',
  'Game of Thrones': 'S06E10',
  'Go On': 'S01E22',
  'Grey\'s Anatomy': 'S07E22',
  'Happy Endings': 'S03E23',
  'Hello Ladies': 'S01E08',
  'Heroes': 'S04E18',
  'Homeland': 'S03E12',
  'House of Cards (2013)': 'S04E13',
  'House of Lies': 'S03E12',
  'House': 'S08E22',
  'How I Met Your Mother': 'S09E24',
  'How to Get Away with Murder': 'S01E15',
  'How to Live With Your Parents': 'S01E13',
  'Joey': 'S02E22',
  'Leo tar valget': 'S01E08',
  'Lilyhammer': 'S02E08',
  'Limitless': 'S01E22',
  'Lost': 'S06E18',
  'Love': 'S01E10',
  'Lyngbø Og Hærlands Big Bang': 'S02E07',
  'Mammon': 'S01E06',
  'Master of none': 'S01E10',
  'Mike & Molly': 'S04E22',
  'Mind Control with Derren Brown': 'S01E06',
  'Modern Family': 'S07E22',
  'Mom': 'S01E22',
  'Mr. Robot': 'S01E10',
  'New Girl': 'S02E25',
  'Numb3rs': 'S06E16',
  'Orange is the New Black': 'S03E13',
  'Outsourced': 'S01E22',
  'Partners (2012)': 'S01E13',
  'Person of Interest': 'S02E22',
  'Rick and Morty': 'S02E10',
  'Schmokk': 'S01E06',
  'Scrubs': 'S09E13',
  'Side om side': 'S03E10',
  'Silicon Valley': 'S02E10',
  'Skam': 'S02E12',
  'South Park': 'S19E10',
  'Suits': 'S05E16',
  'Taxi (2011)': 'S01E04',
  'The Big Bang Theory': 'S09E24',
  'The Cleveland Show': 'S04E23',
  'The Crazy Ones': 'S01E22',
  'The IT Crowd': 'S04E06',
  'The Newsroom (2012)': 'S03E06',
  'The Simpsons': 'S27E22',
  'The Walking Dead': 'S02E13',
  'Titansgrave: The Ashes of Valkana': 'S01E10',
  'Trekant': 'S02E07',
  'Trophy Wife': 'S01E22',
  'Two and a Half Men': 'S12E16',
  'Typen til': 'S01E08',
  'Under the Dome': 'S02E13',
  'Unge lovende': 'S01E06',
  'V (2009)': 'S02E10',
  'Video Game High School (VGHS)': 'S03E06',
  'Whitney': 'S02E16'
};
